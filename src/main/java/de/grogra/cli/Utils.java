package de.grogra.cli;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import org.jline.utils.InfoCmp.Capability;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import de.grogra.cli.ui.CLIWindowSupport;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.expr.StringConst;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.util.ComponentWrapperImpl;
import de.grogra.util.Described;
/**
 * A collection of functions that could be Helpful for the usage of the CLI.
 * @author tim
 *
 */
public class Utils {
	
	public static Object getParameter(Object info, boolean nullable) throws NullPointerException{
		Object o=null;
		if(info instanceof Queue) {
			o = ((Queue) info).poll();
		}
		if(o==null && !nullable) {
			throw new NullPointerException();
		}
		return o;
	}
	
	public static String getStringParameter(Object info, boolean nullable) throws NullPointerException{
		Object o = getParameter(info, nullable);
		String s =null;
		if(o instanceof String) {
			s=(String)o;
		}
		return s;
	}
	
	public static Boolean getBooleanParameter(Object info)  throws NullPointerException{
		return Boolean.parseBoolean(getStringParameter(info,false));
	}
	
	public static int getintParameter(Object info)  throws NullPointerException{
		return Integer.parseInt(getStringParameter(info,false));
	}
	
	public static float getFloatParameter(Object info)  throws NullPointerException{
		return Float.parseFloat(getStringParameter(info,false));
	}
	
	
	public static Path getAbsolutePath(Context ctx, String path) {
		if(path==null) {
			return null;
		}
		CLIApplication app = ((CLIApplication)ctx.getWorkbench().getApplication());
		Path n = new File(path).toPath();
		Path dest = app.getWorkingDirectory().toPath().resolve(n);
		dest = dest.normalize();
		return dest;
	}
	
	
	public static void changeDirectory(Item item, Object info, Context ctx) {
		try {
			String link =getStringParameter(info,false);
			CLIApplication app = ((CLIApplication)ctx.getWorkbench().getApplication());
			Path dest = getAbsolutePath(ctx,link);
			File f = dest.toFile();
			if(f.exists()) {
				app.setWorkingDirectory(f);
				consoleWrite(ctx, dest.toString());
			}else {
				consoleWrite(ctx, CLIApplication.I18N.msg("error.fs.directorynotfound"));
			}
		}catch(NullPointerException e) {
			consoleWrite(ctx,CLIApplication.I18N.msg("error.fs.missingargument"));
		}
	}

	public static void listDirectoryContent(Item item, Object info, Context ctx) {
		String result="";
		for(File f : ((CLIApplication)ctx.getWorkbench().getApplication()).getWorkingDirectory().listFiles()) {
			result+=f.getName()+"\n";
		}
		consoleWrite(ctx, result);
	}

	/**
	 * Commands that handle the selected directory functions.
	 */
	public static void printDirectoryPath(Item item, Object info, Context ctx) {
		String result = ((CLIApplication)ctx.getWorkbench().getApplication()).getWorkingDirectory().getAbsolutePath();
		consoleWrite(ctx, result);
	}

	
	public static void editFile(Item item, Object info, Context ctx) {
	try {
		String link =getStringParameter(info,false);
		CLIApplication app = ((CLIApplication)ctx.getWorkbench().getApplication());
		Path dest = getAbsolutePath(ctx,link);
		File f = dest.toFile();
		if(f.exists()) {
			try {
				Desktop.getDesktop().open(f);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else {
			consoleWrite(ctx, "file not found");
		}
	}catch(NullPointerException e) {
		consoleWrite(ctx,"please enter a relative path to a file");
	}
	}
	
	
	public static void consoleWrite(Context ctx, Object o) {
		((CLIWindowSupport)ctx.getWindow()).consoleWrite(new ComponentWrapperImpl(o, null));
	}
	
	public static void clearConsole(Item item, Object info, Context ctx) {
		((CLIApplication)ctx.getWorkbench().getApplication()).terminal.puts(Capability.clear_screen);
		((CLIApplication)ctx.getWorkbench().getApplication()).terminal.flush();
	}
	
	
	public static void consoleWriteList(Context ctx, Object[] list, boolean numerated) {
		String text="";
		int i=0;
		for(Object item :list) {
			text+=numerated?(i++)+" : ":"";
			text+=item+System.lineSeparator();
		}
		consoleWrite(ctx,text+System.lineSeparator());
	}
	
	public static void printHeader() {
		System.out.println(
"  _____            _____ _      _____ __  __ _____  "+System.lineSeparator()+
" / ____|          / ____| |    |_   _|  \\/  |  __ \\ "+System.lineSeparator()+
"| |  __ _ __ ___ | |    | |      | | | \\  / | |__) |"+System.lineSeparator()+
"| | |_ | '__/ _ \\| |    | |      | | | |\\/| |  ___/ "+System.lineSeparator()+
"| |__| | | | (_) | |____| |____ _| |_| |  | | |     "+System.lineSeparator()+
" \\_____|_|  \\___/ \\_____|______|_____|_|  |_|_|     "+System.lineSeparator()
);}
	
	
	public static void printStartHelp() {
		System.out.println(
" ~ HELP: You can open your GroIMP project with the command: "+System.lineSeparator()+
" ~ \"$open path/to/your/project.gsz\""+System.lineSeparator()+
" ~ Then, you can run any method from the project (e.g. run)"+System.lineSeparator()+
" ~ Get the complete list of command with \"$help \" "+System.lineSeparator()
);}
	
	
	//TODO: move from here (probably put in pf.io
	//TODO: I think there is a method in String or in Path that does exactly this
	public static String getExtension(String path) {
		return path.substring(path.lastIndexOf('.') + 1);
	}
		
	public static String setExtension(String path, String ext) {
		if (getExtension(path).endsWith(ext) || ext.length()<=0)
			return path;
		if (ext.charAt(0) != '.' && ext.length()>=1) 
			return path.concat("."+ext);
		if (ext.charAt(0) == '.' && ext.length()>1)
			return path.concat(ext);
		return path;
	}
	
	
	static class HelpSet{
		String name;
		String parameter;
		String description;
		public HelpSet(String name, String parameter, String description) {
			this.name=name;
			this.description=description;
			this.parameter=parameter;
		}
		public String toString() {
			if(parameter!=null) {
				name+=" ("+parameter+")";
			}
			String space ="";
			for(int i=0; i< 40-name.length();i++) {
				space+=" ";
			}
			return name+space+description +System.lineSeparator();
		}
	}
	
	
	
	private static String item2helpString(Item f,String dir, String pre) {
		String name = f.getAbsoluteName().substring(dir.length() + 1);
		if(name.contains("/")) {
			return "";
		}
		String param =(String)f.getDescription("Parameter");
		if(param!=null) {
			name+=" ("+param+")";
		}
		String space ="";
		for(int i=0; i< 41-name.length();i++) {
			space+=" ";
		}
		space=(pre.length()==0)?space+" ":space;
		String description = (String) f.getDescription(Described.SHORT_DESCRIPTION);
		
		String descName= (String) f.getDescription(Described.NAME);
		if(descName.equals(description)) {
			return "";
		}
		
		
		return pre+name+space+description +System.lineSeparator();
	}
	
	

	/**
	 * a new criteria was necessary to also list the shortcuts
	 */
	static ItemCriterion INSTANCE_OF_ANY
	= new ItemCriterion ()
		{
			public boolean isFulfilled (Item item, Object info)
			{
				for(Class c : (Class[])info) {
					if(c.isInstance(item)) {
						return true;
					}
				}
				
				return false;
			}


			public String getRootDirectory ()
			{
				return null;
			}
		};
	static HelpSet[] windowCommands = new HelpSet[] {new HelpSet("%inspector:select","tree-id","select the node"),new HelpSet("%inspector:show","tree-id","opens the properties of the node"),new HelpSet("%log",null,"show the log"),new HelpSet("%fileexplorer",null,"list all files"), new HelpSet("%fileexpolorer:show","file name or id","opens a file in nano")};
		
		
	public static void showHelp(Item item, Object info, Context ctx) {
		CLIApplication app = ((CLIApplication) ctx.getWorkbench().getApplication());
		String help = "";
		help = "Help:" + System.lineSeparator() + "The CLI is designed to execute commands on the "
				+ System.lineSeparator() + "individual workbench as well as on the whole app." + System.lineSeparator()+ System.lineSeparator();
		app.print(help);
		String result = "App commands ($)" + System.lineSeparator();
		for (String dir : CLIApplication.APPCOMMANDDIRS) {
			Item fileDir = Item.resolveItem(app.getRegistry(), dir);
			Item[] files = Item.findAll(fileDir,INSTANCE_OF_ANY, new Class[] {Command.class, StringConst.class}, true);
			for (Item f : files) {
				result +=item2helpString(f,dir,"$");
				}
		}
		for (String dir : CLIApplication.APPCOMMANDDIRS) {
			Item fileDir = Item.resolveItem(app.getMainWorkbench(), dir);
			Item[] files = Item.findAll(fileDir,INSTANCE_OF_ANY, new Class[] {Command.class, StringConst.class}, true);
			for (Item f : files) {
				result +=item2helpString(f,dir,"$");
			}
		}

		result += System.lineSeparator() + "Window command (%)" + System.lineSeparator() +" only a small selection of the most used panels..."+System.lineSeparator();
		for (String dir : app.WINDOWCOMMANDDIRS) {
			Item fileDir = Item.resolveItem(app.selected.getRegistry(), dir);
			Item[] files = Item.findAll(fileDir,INSTANCE_OF_ANY, new Class[] {Command.class, StringConst.class}, true);
			for (Item f : files) {
				result +=item2helpString(f,dir,"%");
			}
		}
		for(HelpSet h : windowCommands) {
			result+=h;
		}
		result += System.lineSeparator() + "Workbench command " + System.lineSeparator();

		for (String dir : CLIApplication.WORKBENCHCOMMANDDIRS) {
			Item fileDir = Item.resolveItem(app.selected.getRegistry(), dir);
			Item[] files = Item.findAll(fileDir,INSTANCE_OF_ANY, new Class[] {Command.class, StringConst.class}, true);
			for (Item f : files) {
				result += item2helpString(f,dir,"");

			}
		}
		for (String dir : CLIApplication.WORKBENCHCOMMANDDIRS) {
			Item fileDir = Item.resolveItem(app.getRegistry(), dir);
			Item[] files = Item.findAll(fileDir,INSTANCE_OF_ANY, new Class[] {Command.class, StringConst.class}, true);
			for (Item f : files) {
				result += item2helpString(f,dir,"");

			}
		}
		consoleWrite(ctx,result);
	}
	
	
	public static List<String> getAllCommands(CLIApplication app) {
		List<String> result = new ArrayList<String>();

		for (String dir : CLIApplication.APPCOMMANDDIRS) {
			Item fileDir = Item.resolveItem(app.getRegistry(), dir);
			Item[] files = Item.findAll(fileDir,INSTANCE_OF_ANY, new Class[] {Command.class, StringConst.class}, true);
			for (Item f : files) {
				result.add("$" + f.getAbsoluteName().substring(dir.length() + 1));
			}
		}
		for (String dir : CLIApplication.APPCOMMANDDIRS) {
			Item fileDir = Item.resolveItem(app.getMainWorkbench(), dir);
			Item[] files = Item.findAll(fileDir,INSTANCE_OF_ANY, new Class[] {Command.class, StringConst.class}, true);
			for (Item f : files) {
				result.add("$" + f.getAbsoluteName().substring(dir.length() + 1));
			}
		}
		for (String dir : CLIApplication.WINDOWCOMMANDDIRS) {
			Item fileDir = Item.resolveItem(app.selected.getRegistry(), dir);
			Item[] files = Item.findAll(fileDir,INSTANCE_OF_ANY, new Class[] {Command.class, StringConst.class}, true);
			for (Item f : files) {
				result.add("%" + f.getAbsoluteName().substring(dir.length() + 1));
			}
		}

		for (String dir : CLIApplication.WORKBENCHCOMMANDDIRS) {
			Item fileDir = Item.resolveItem(app.selected.getRegistry(), dir);
			Item[] files = Item.findAll(fileDir,INSTANCE_OF_ANY, new Class[] {Command.class, StringConst.class}, true);
			for (Item f : files) {
				result.add(f.getAbsoluteName().substring(dir.length() + 1) );
			}
		}
		for (String dir : CLIApplication.WORKBENCHCOMMANDDIRS) {
			Item fileDir = Item.resolveItem(app.getRegistry(), dir);
			Item[] files = Item.findAll(fileDir,INSTANCE_OF_ANY, new Class[] {Command.class, StringConst.class}, true);
			for (Item f : files) {
				result.add(f.getAbsoluteName().substring(dir.length() + 1));
			}
		}
		return result;
	}
	
	public static List<String> getWindowCommands(CLIApplication app) {
		List<String> result = new ArrayList<String>();
		
		for (String dir : CLIApplication.WINDOWCOMMANDDIRS) {
			Item fileDir = Item.resolveItem(app.selected.getRegistry(), dir);
			Item[] files = Item.findAll(fileDir, INSTANCE_OF_ANY, new Class[] {Command.class, StringConst.class}, true);
			for (Item f : files) {
				result.add("%" + f.getAbsoluteName().substring(dir.length() + 1));
			}
		}
		return result;
	}
	
	public static List<String> getWorkbenchCommands(CLIApplication app) {
		List<String> result = new ArrayList<String>();
		
		for (String dir : CLIApplication.WORKBENCHCOMMANDDIRS) {
			Item fileDir = Item.resolveItem(app.selected.getRegistry(), dir);
			Item[] files = Item.findAll(fileDir, INSTANCE_OF_ANY, new Class[] {Command.class, StringConst.class}, true);
			for (Item f : files) {
				result.add(f.getAbsoluteName().substring(dir.length() + 1) );
			}
		}
		return result;
	}

	public static List<String> getAppCommands(CLIApplication app) {
		List<String> result = new ArrayList<String>();
		for (String dir : CLIApplication.APPCOMMANDDIRS) {
			Item fileDir = Item.resolveItem(app.getRegistry(), dir);
			Item[] files = Item.findAll(fileDir, INSTANCE_OF_ANY, new Class[] {Command.class, StringConst.class}, true);
			for (Item f : files) {
				result.add("$" + f.getAbsoluteName().substring(dir.length() + 1));
			}
		}
		for (String dir : CLIApplication.APPCOMMANDDIRS) {
			Item fileDir = Item.resolveItem(app.getMainWorkbench(), dir);
			Item[] files = Item.findAll(fileDir, INSTANCE_OF_ANY, new Class[] {Command.class, StringConst.class}, true);
			for (Item f : files) {
				result.add("$" + f.getAbsoluteName().substring(dir.length() + 1));
			}
		}
		return result;
	}
}







