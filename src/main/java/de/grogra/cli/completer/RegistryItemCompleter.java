package de.grogra.cli.completer;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.jline.reader.Candidate;
import org.jline.reader.LineReader;
import org.jline.reader.ParsedLine;

public class RegistryItemCompleter implements org.jline.reader.Completer{
	
	private Collection<String> commands;
	
	class Separator{
		int pos;
		String sep;
		
		Separator(){
			this(-1,"/");
		}
		Separator(int i, String c){
			pos=i;
			sep=c;
		}
		public int getPos() {
			return pos;
		}
		public String getSep() {
			return sep;
		}
		public void getLastIndexOf(String path, String[] seps) {
			int tmp = pos = -1;
//			sep="/";
			for (String sep : seps) {
				tmp = path.lastIndexOf(sep);
				if (tmp>pos) {
					pos=tmp;
					this.sep=sep;
				}
			}
		}
		public void getIndexOf(String path, String[] seps) {
			int tmp = pos = -1;
//			sep="/";
			for (String sep : seps) {
				tmp = path.indexOf(sep);
				if (tmp>pos) {
					pos=tmp;
					this.sep=sep;
				}
			}
		}
	}
		
    public RegistryItemCompleter(String... strings) {
        this(Arrays.asList(strings));
    }

    public RegistryItemCompleter(Iterable<String> strings) {
        assert strings != null;
        this.commands = new ArrayList<>();
        for (String string : strings) {
        	commands.add(string);
        }
    }
    
    public void setCommands(Iterable<String> strings) {
    	assert strings != null;
        this.commands = new ArrayList<>();
        for (String string : strings) {
        	commands.add(string);
        }
    }
	
	public void complete(LineReader reader, ParsedLine commandLine, final List<Candidate> candidates) {
        assert commandLine != null;
        assert candidates != null;
        
        Set<String> toAdd = new HashSet<String>();

        String buffer = commandLine.word().substring(0, commandLine.wordCursor());
        String curBuf;
        String[] seps = {"/", ":"};
        Separator separator = new Separator();
        separator.getLastIndexOf(buffer, seps);
        int lastSep = separator.getPos();
        String sep = separator.getSep();
        try {
            if (lastSep >= 0) {
                curBuf = buffer.substring(0, lastSep + 1);
            } else {
                curBuf = "";
            }
            
            for (String cmd : commands) {
            	if (accept(cmd, curBuf)) {
            		String futureCandidate = cmd.substring(curBuf.toString().length());
            		separator.getIndexOf(futureCandidate, seps);
            		int cmdLastSep = separator.getPos();
            		sep = separator.getSep();
            		if (cmdLastSep >= 0) {
            			futureCandidate = futureCandidate.substring(0, cmdLastSep+1);
            		}
            		toAdd.add(futureCandidate);
            	}
            }
            for (String c : toAdd) {
            	if (c.endsWith(sep)) {
            		candidates.add(new Candidate(curBuf+c, c,null, null, sep, null, false));
            	}
            	else
            		candidates.add(new Candidate(curBuf+c, c,null, null, null, null, true));
            }
        }catch(Exception e) {}
    }

    public Path getRootDir() {
		return Paths.get("");
	}
	
	protected boolean accept(String isIn, String path) {
		return isIn.startsWith(path);
    }
}