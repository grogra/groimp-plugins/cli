package de.grogra.cli.completer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jline.reader.impl.completer.ArgumentCompleter;
import org.jline.reader.impl.completer.NullCompleter;

import de.grogra.cli.CLIApplication;
import de.grogra.cli.Utils;

public class ApplicationCompleter extends ArgumentCompleter{

	
	public ApplicationCompleter(CLIApplication app) {
		
		List<String> cmdAppWithWorkbenchArgs = new ArrayList<String>();
		List<String> cmdAppWithFilesArgs = new ArrayList<String>();
		List<String> cmdApp = Utils.getAppCommands(app);
		//remove open, close & select
		for (String s : cmdApp) {
			if (s.endsWith("selectWB") || s.endsWith("close")) {
				cmdAppWithWorkbenchArgs.add(s);
			}
			if (s.endsWith("open") || s.endsWith("cd") || s.endsWith("view")) {
				cmdAppWithFilesArgs.add(s);
			}
		}
		cmdApp.removeAll(cmdAppWithFilesArgs);
		cmdApp.removeAll(cmdAppWithWorkbenchArgs);
		
		RegistryItemCompleter commandsApp = new RegistryItemCompleter(cmdApp);
		
		this.getCompleters().addAll(Arrays.asList(commandsApp, NullCompleter.INSTANCE));
	}
		
}
