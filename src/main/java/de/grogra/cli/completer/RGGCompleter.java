package de.grogra.cli.completer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jline.reader.Candidate;
import org.jline.reader.Completer;
import org.jline.reader.impl.completer.ArgumentCompleter;
import org.jline.reader.impl.completer.NullCompleter;
import org.jline.reader.impl.completer.StringsCompleter;
import org.jline.utils.AttributedString;

import de.grogra.cli.CLIApplication;
import de.grogra.cli.Utils;
import de.grogra.pf.ui.ProjectWorkbench;
import de.grogra.pf.ui.UIApplication;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.WorkbenchManager;
import de.grogra.projectmanager.ProjectImpl;
import de.grogra.pf.ui.Command;

public class RGGCompleter extends ArgumentCompleter{

	ListRGGCompleter listRGGCmd ;

	public RGGCompleter(CLIApplication app) {
		
		listRGGCmd = new ListRGGCompleter();
		ListRGGCompleter cmdRGG = listRGGCmd;

		this.getCompleters().addAll(Arrays.asList(cmdRGG, NullCompleter.INSTANCE));
		
	}
	
	
	public class ListRGGCompleter extends StringsCompleter{
		public void setCandidate(String... strings) {
			setCandidate(Arrays.asList(strings));
	    }

	    public void setCandidate(Iterable<String> strings) {
	        assert strings != null;
	        this.candidates = null;
	        this.candidates = new ArrayList<>();
	        for (String string : strings) {
	            candidates.add(new Candidate(AttributedString.stripAnsi(string), string, null, null, null, null, true));
	        }
	    }
	}
	
	public void setRGGCommands(Iterable<String> strings) {
		if (listRGGCmd!=null)
			listRGGCmd.setCandidate(strings);
	}
	
	public void setRGGCommands(String... strings) {
		setRGGCommands(Arrays.asList(strings));
	}
	
	public void setRGGCommands(Command[] commandsList) {
		List<String> names = new ArrayList<String>();
		for (Command cmd : commandsList) {
			names.add(cmd.getCommandName());
		}
		setRGGCommands(names);
	}
	
	public Completer getRGGCompleter() {
		return listRGGCmd;
	}
}
