package de.grogra.cli.completer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jline.reader.Candidate;
import org.jline.reader.Completer;
import org.jline.reader.impl.completer.ArgumentCompleter;
import org.jline.reader.impl.completer.NullCompleter;
import org.jline.reader.impl.completer.StringsCompleter;
import org.jline.utils.AttributedString;

import de.grogra.cli.CLIApplication;
import de.grogra.cli.Utils;
import de.grogra.pf.ui.ProjectWorkbench;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.WorkbenchManager;

public class ApplicationOnWorkbenchCompleter extends ArgumentCompleter{

	ListWorbenchesCompleter listWorkbenches ;
	
	public ApplicationOnWorkbenchCompleter(CLIApplication app) {
		listWorkbenches = new ListWorbenchesCompleter();
		ListWorbenchesCompleter arguments = listWorkbenches;
		
		List<String> cmdAppWithWorkbenchArgs = new ArrayList<String>();
		List<String> cmdApp = Utils.getAppCommands(app);
		//remove open, close & select
		for (String s : cmdApp) {
			if (s.endsWith("selectWB") || s.endsWith("closeWB")) {
				cmdAppWithWorkbenchArgs.add(s);
			}
		}
		cmdApp.removeAll(cmdAppWithWorkbenchArgs);
		
		RegistryItemCompleter commandsWithWorkbenchArgs = new RegistryItemCompleter(cmdAppWithWorkbenchArgs);
		
		this.getCompleters().addAll(Arrays.asList(commandsWithWorkbenchArgs, arguments, NullCompleter.INSTANCE));
		
	}
	
	public class ListWorbenchesCompleter extends StringsCompleter{
		public void setCandidate(String... strings) {
			setCandidate(Arrays.asList(strings));
	    }

	    public void setCandidate(Iterable<String> strings) {
	        assert strings != null;
	        this.candidates = null;
	        this.candidates = new ArrayList<>();
	        for (String string : strings) {
	            candidates.add(new Candidate(AttributedString.stripAnsi(string), string, null, null, null, null, true));
	        }
	    }
	}
	
	public void setWorkbenches(Iterable<String> strings) {
		if (listWorkbenches!=null)
		listWorkbenches.setCandidate(strings);
	}
	
	public void setWorkbenches(String... strings) {
		setWorkbenches(Arrays.asList(strings));
	}
	
	public void setWorkbenches(ArrayList<Workbench> workbenches) {
		List<String> names = new ArrayList<String>();
		if ( workbenches.size() > 0 ) {
			WorkbenchManager wbm = workbenches.get(0).getApplication().getWorkbenchManager();
			workbenches.forEach( (wb) -> names.add( String.valueOf( wbm.getWorkbenchId((ProjectWorkbench)wb) ) ));
		}
		setWorkbenches(names);
	}
	
	public Completer getWorkbenchCompleter() {
		return listWorkbenches;
	}
}