package de.grogra.cli.completer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jline.reader.impl.completer.ArgumentCompleter;
import org.jline.reader.impl.completer.NullCompleter;
import org.jline.reader.impl.completer.StringsCompleter;

import de.grogra.cli.CLIApplication;
import de.grogra.cli.Utils;
import de.grogra.pf.ui.UIApplication;

public class WorkbenchCompleter extends ArgumentCompleter{

//	public WorkbenchCompleter(UIApplication app) {
//		super(Utils.getWorkbenchCommands((CLIApplication)app));
//	}
	
	public WorkbenchCompleter(CLIApplication app) {
		
		List<String> cmdWb = Utils.getWorkbenchCommands((CLIApplication)app);
		
		RegistryItemCompleter commandsWb = new RegistryItemCompleter(cmdWb);
		
		this.getCompleters().addAll(Arrays.asList(commandsWb, NullCompleter.INSTANCE));
		
	}
}
