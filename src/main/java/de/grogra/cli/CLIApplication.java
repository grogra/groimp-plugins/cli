package de.grogra.cli;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantLock;

import org.jline.builtins.Nano;
import org.jline.reader.Completer;
import org.jline.reader.EndOfFileException;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.Parser;
import org.jline.reader.UserInterruptException;
import org.jline.reader.LineReader.Option;
import org.jline.reader.impl.DefaultParser;
import org.jline.reader.impl.LineReaderImpl;
import org.jline.reader.impl.completer.AggregateCompleter;
import org.jline.reader.impl.completer.StringsCompleter;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;

import de.grogra.cli.completer.WindowCompleter;
import de.grogra.cli.completer.WorkbenchCompleter;
import de.grogra.cli.completer.ApplicationCompleter;
import de.grogra.cli.completer.ApplicationOnFileCompleter;
import de.grogra.cli.completer.ApplicationOnWorkbenchCompleter;
import de.grogra.cli.completer.RGGCompleter;
import de.grogra.cli.completer.SystemFileCompleter;
import de.grogra.cli.ui.CLIConsolePanel;
import de.grogra.pf.boot.Main;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.registry.Application;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.expr.StringConst;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.FileChooserResult;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.ProjectManager;
import de.grogra.pf.ui.ProjectWorkbench;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.UIApplication;
import de.grogra.pf.ui.Window;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.WorkbenchManager;
import de.grogra.pf.ui.registry.FilterSourceFactory;
import de.grogra.pf.ui.registry.PanelFactory;
import de.grogra.pf.ui.registry.ProjectDirectory;
import de.grogra.projectmanager.ProjectFactoryImpl;
import de.grogra.projectmanager.ProjectManagerImpl;
import de.grogra.util.Described;
import de.grogra.util.I18NBundle;
import de.grogra.util.Map;
import de.grogra.util.MimeType;

public class CLIApplication extends UIApplication {
	private static CLIApplication PLUGIN;
	public static final I18NBundle I18N = I18NBundle.getInstance (CLIApplication.class);
	private File workingDirectory;
	private boolean running;

	/**
	 * Possible states in which the current readline operation may be in.
	 */
	public enum State {
		/**
		 * The application is waiting for the user to write a command
		 */
		READ_COMMAND,
		/**
		 * A dialog has been open and the user needs to write a value
		 */
		READ_VALUE,
		/**
		 * the application is opening a text editor, been unavailable while its open
		 */
		READ_TEXTEDITOR,
		/**
		 * the terminal is set as a xlconsole linked to the selected workbench - all commands are pushed to the xlconsole
		 * until exit
		 */
		READ_XLCONSOLE,
		
	}

	protected State state = State.READ_COMMAND;
	// TODO: check if an additional lock is required. The jobmanager of the
	// application should be enough

	// lock on the terminal for writing
	protected final ReentrantLock allowToWrite = new ReentrantLock();

	static String[] APPCOMMANDDIRS = new String[] { "/ui/commands/app" };
	static String[] WINDOWCOMMANDDIRS = new String[] { "/ui/commands/window", "/ui/panels" };
	static String[] WORKBENCHCOMMANDDIRS = new String[] { "/ui/commands", "/workbench/rgg/methods" };

	// Jline objects
	Terminal terminal = null;
	TerminalBuilder builder = TerminalBuilder.builder();
	// completer for the file system paths
	private Completer sysFileCompleter = null;
	// completer for the groimp app/ windows/ & workbench commands
	Completer appCompleter = null;
	Completer appWbCompleter =null;
	Completer appFileCompleter=null;
	Completer workbenchCompleter = null;
	Completer windowCompleter = null;
	Completer xlConsoleCompleter=null;
	Completer rggCompleter=null;
	Completer cliCompleter = null;
	Completer readerCompleter=null;
	Parser parser = null;
	LineReader reader;
	CLIStringWritter printAbove;
	String PROMPT;
	String RIGHT_PROMPT="DEFAULT_S";
	
	// nano variables
	File fileToEdit;
	String fileToEdit_RealName; 
	
	// xlconsole objects
	final String XLPROMPT = "XLCODE >";
	final String XLEXIT = "exit";
	
	// application key chars
	final char APP_CHAR = '$';
//	final char WB_CHAR = ' ';
	final char WINDOW_CHAR = '%';
	final char LOCK_CHAR = '&';
	final char PIPE_CHAR = '|';
	final char WINDOW_PIPE_CHAR = ':';
	

	Object BLOCKING = new Object();
	Command WAIT_BLOCK = new Command() {

		@Override
		public String getCommandName() {
			return null;
		}

		@Override
		public void run(Object info, Context context) {
			synchronized(BLOCKING) {
				BLOCKING.notifyAll();
			}
		}
	};
	
	public CLIApplication() {
		assert PLUGIN == null;
		PLUGIN = this;
	}

	public static CLIApplication getInstance() {
		return PLUGIN;
	}

	public Workbench getCurrentWorkbench() {
		return selected;
	}

	class Request {
		String query; // the initial line string
		Command command; // the command behind the first word
		String[] baseDirs; // the list of command directories based on the first char (wb, app, ..)
		String panelExec; // an additional executable to be applied on a panel (optional)
		Queue<Object> paramqueue; //a queue of additional parameters to be used by the command
		ProjectWorkbench wb; // the workbench to use for the execution
		boolean doLocklely; // should the cmd lock the terminal? 
		public Request(String query, Command cmd, String[] commandDirs, String panelExec,
				ProjectWorkbench usedWB, Queue<Object> paramqueue, boolean doLocklely) {
			this.query = query;
			this.command = cmd;
			this.baseDirs = commandDirs;
			this.panelExec = panelExec;
			this.wb=usedWB;
			this.paramqueue=paramqueue;
			this.doLocklely=doLocklely;
		}
		public Request() {
			this.query = "";
			this.command=null;
			this.panelExec="";
			this.baseDirs = new String[0];
			this.wb=getMainWorkbench();
			this.doLocklely=false;
		}
	}

	@Override
	public String getCommandName() {
		return "GroCLIMP";
	}

	public File getWorkingDirectory() {
		return workingDirectory;
	}

	/**
	 * requires a mainworkbench
	 * 
	 * @param workingDirectory
	 */
	public void setWorkingDirectory(File workingDirectory) {
		this.workingDirectory = workingDirectory;
		getMainWorkbench().setProperty(Workbench.CURRENT_DIRECTORY, workingDirectory.getAbsolutePath().toString());
	}

	public static void run(Application app) {

		ProjectManager pm = ProjectManagerImpl.getInstance();
		CLIToolkit ui = new CLIToolkit();
		WorkbenchManager wbm = new CLIWorkbenchManager(Registry.create(app.getRegistry()), PLUGIN);
		PLUGIN.init(pm, ui, wbm);
		PLUGIN.startUIApp();
		PLUGIN.runUIApp();
		System.out.println(I18N.msg("exit.msg"));
//		Utils.printHeader();
	}

	public void run(Object arg, Context context) {

	}

	protected void startUIApp() {
		Main.closeSplashScreen();
		running = true;
		try {
			setMainWorkbench((CLIWorkbench) wbm.createWorkbench(new ProjectFactoryImpl()));
		} catch (IOException e1) {
			System.out.println(I18N.msg("error.start.mainworkbench"));
			Main.exit();
		}
		getMainWorkbench().getProject().setName("base");
		selected = getMainWorkbench();
		setWorkingDirectory(new File(System.getProperty("user.home")));
		System.out.println(I18N.msg("start.msg"));
		Utils.printHeader();
		Utils.printStartHelp();

		// init jline
		builder.system(true);
		//TODO: xlconsole completer 
		xlConsoleCompleter=new StringsCompleter();
		cliCompleter = setupCompleters();
		setCLICompleter();
		DefaultParser p3 = new DefaultParser();
		p3.setEscapeChars(new char[] {});
		parser = p3;
		builder.name("GroIMP");
		try {
			terminal = builder.build();
		} catch (IOException e) {
			e.printStackTrace();
		}

		reader = LineReaderBuilder.builder().terminal(terminal).completer(readerCompleter).parser(parser)
				.variable(LineReader.SECONDARY_PROMPT_PATTERN, "%M%P > ").variable(LineReader.INDENTATION, 2)
				.option(Option.INSERT_BRACKET, true).build();
		printAbove = new CLIStringWritter(reader);
	}

	private void setState(State newState) {
		state = newState;
	}
	
	public synchronized void println(String out) {
		if (out!=null)
		printAbove.write(out+System.lineSeparator());
	}

	public synchronized void print(String out) {
		if (out!=null)
		printAbove.write(out);
	}

	public synchronized void println(Object out) {
		if (out!=null)
		terminal.writer().println(out);
	}

	public synchronized void print(Object out) {
		if (out!=null)
		terminal.writer().print(out);
	}
	
	public synchronized void flush() {
		if (terminal!=null)
			terminal.flush();
		printAbove.flush();
	}

	// print a different prompt when a workbench is workking
	public void updateLoadingPrompt(String rightPrompt) {
		if (rightPrompt!=null) {
			RIGHT_PROMPT= rightPrompt;
			((LineReaderImpl)reader).setRightPrompt(RIGHT_PROMPT);
			((LineReaderImpl)reader).redisplay();
		}
	}
	
	public void updatePrompt() {
		String busy = ( selected.getJobManager()
				.isExecuting())?"~R":"";
		setPROMPT("[" + selected.getName() + busy +"]>>");
	}
	
	// COMPLETERS 
	private void updateXlCompleter() {
		
	}
	
	private void setXlCompleter() {
		readerCompleter=xlConsoleCompleter;
		updateReader();
	}
	
	private void setCLICompleter() {
		readerCompleter=cliCompleter;
		updateReader();
	}
	
	private void updateLineCompleter(File newDir) {
		if (reader==null) {return;}
		((SystemFileCompleter) getSysFileCompleter()).changeDir(newDir);
	}
	
	public void updateCLICompleters() {
		// update the system file path
		updateLineCompleter(this.workingDirectory);
		// update the list of selectable wb
		((ApplicationOnWorkbenchCompleter) appWbCompleter).setWorkbenches(wbm.getWorkbenches());
		// update the list of selectable wb
		((RGGCompleter) rggCompleter).setRGGCommands( selected.getFunctions()  );		
	}
	
	public Completer setupCompleters() {
		sysFileCompleter = new SystemFileCompleter(this.workingDirectory);
		appCompleter = new ApplicationCompleter(this);
		appWbCompleter = new ApplicationOnWorkbenchCompleter(this);
		appFileCompleter = new ApplicationOnFileCompleter(this);
		workbenchCompleter = new WorkbenchCompleter(this);
		windowCompleter = new WindowCompleter(this);
		rggCompleter = new RGGCompleter(this);

		return new AggregateCompleter(appCompleter, appWbCompleter, appFileCompleter,
				windowCompleter, workbenchCompleter, rggCompleter );
	}
	
	public void updateReader() {
		if (reader==null) {return;}
		((LineReaderImpl)reader).setCompleter(readerCompleter);
	}

	public void setPROMPT(String s) {
		PROMPT = s;
	}
	
	
	public void setFileToEdit(File f,String n) {
		fileToEdit = f;
		fileToEdit_RealName=n;
	}
	
	/**
	 * Check if the thread is allowed to readlines for requesting values in the
	 * terminal Only the commands that blocks the terminal should be allowed to
	 * write value
	 * 
	 */
	public boolean canReadValue() {
		if (allowToWrite.tryLock()) {
			try {
				return state == State.READ_VALUE;
			} finally {
				allowToWrite.unlock();
			}
		} else {
			return false;
		}
	}

	private void runUIApp() {
		while (running) {
			try {
			getTerminalInput();
			} catch (UserInterruptException e) {
				exit();
			} catch (EndOfFileException e) {
				exit();
			}
			catch (Exception e) {
				System.out.println(e);
				println(e);
			}
		}
	}

	/**
	 * exit the application
	 */
	public void exit() {
		if (!running) {return;}
		Workbench w;
		synchronized (wbm.getWorkbenches()) {
			if (wbm.listWorkbenches().size() == 0) {
				running = false;
				return;
			}
			w = (CLIWorkbench) getMainWorkbench();
		}
		w.getJobManager().execute(Workbench.CLOSE, new Command() {
			@Override
			public void run(Object i, Context c) {
				exit();
			}

			@Override
			public String getCommandName() {
				return null;
			}
		}, w, JobManager.UI_PRIORITY);
		running = false;
	}

	public Object getTerminalInput() throws UserInterruptException, EndOfFileException {
		Object input;
		switch (state) {
		case READ_COMMAND:
			input = consoleReadCommand();
			break;
		case READ_VALUE:
			input = consoleReadValue();
			break;
		case READ_TEXTEDITOR:
			input=consoleEditNano();
			break;
		case READ_XLCONSOLE:
			input = consoleReadXLConsole();
			break;
		default:
			input = null;
		}
		return input;
	}


	private Object consoleReadValue() throws UserInterruptException, EndOfFileException {
		String line = null;
		line = reader.readLine(PROMPT);
		line = line.trim();
		return line;
	}

	private Object consoleReadCommand() throws UserInterruptException, EndOfFileException {
		String line = null;		
		updatePrompt();
		line = reader.readLine(PROMPT);
		line = line.trim();
		Request r =  parseRequest(line);
		handleRequest(r);
		return line;
	}
	
	private Object consoleEditNano() throws UserInterruptException, EndOfFileException{
		try {
			if(terminal.getType() == Terminal.TYPE_DUMB) {
				print(I18N.msg("error.nano.dumbterminal"));
				flush();
				return null;
			}
			Nano edit = new Nano(terminal, workingDirectory.toPath());
			System.out.println(fileToEdit.getAbsolutePath());
			edit.open(fileToEdit.getAbsolutePath());
			edit.run();

		} catch (IOException e) {
			print(I18N.msg("error.nano.filenotfound"));
		}
		return null;
	}

	private Object consoleReadXLConsole() {
		String line = "";
		
		setXlCompleter();
		CLIConsolePanel c = (CLIConsolePanel) PanelFactory.getAndShowPanel(selected, "/ui/panels/rgg/console",
				Map.EMPTY_MAP);
		
		while (!line.equals(XLEXIT)) {
			line = reader.readLine(XLPROMPT);
			line = line.trim();
			if(!line.equals(XLEXIT))
			c.enter(line + "\n");
		}
		setCLICompleter();
		return line;
	}


	public void startConsoleFileEditor() throws UserInterruptException, EndOfFileException {
		setState(State.READ_TEXTEDITOR);
		getTerminalInput();
	}
	
	/**
	 * This redispatch the command line into one of the three cases depending on the
	 * initial char: 1) $ : the command is an Application commands 2) % : the
	 * command is a window command. The window support used is the one of the
	 * currently selected wb 3) (empty) : the command is a workbench command More
	 * info on each set of commands with the commands $help, %help, and help. It is
	 * also possible to execute commands with the absolute path of the command in
	 * the registry.
	 * 
	 * @param query: the string input from the console
	 */
	private Request parseRequest(String query) {
		if (query.length() < 1) {
			return new Request();
		}
		// set the run mode to locked or not locked - 
		// the lock is on the "handler" the one who read the cmd - not execute it
		boolean doLocklely = true;
		if (query.endsWith(String.valueOf(LOCK_CHAR))) {
			doLocklely = false;
			query = query.substring(0, query.length() - 1);
			query.trim();
		}
		String[] commandDirs;
		ProjectWorkbench usedWB;
		if (getPrefiexes(getMainWorkbench()).contains(query.charAt(0))) {
			commandDirs = getCommandDirs(query.charAt(0));
			usedWB = getUsedWB(query.charAt(0));
			query = query.substring(1);
		} else {
			commandDirs = WORKBENCHCOMMANDDIRS;
			usedWB = selected;
		}
		
		query = query.trim();
		if (query.length() == 0) {
			return new Request();
		}

		String[] splitted = query.split(" ");
		String command = splitted[0];
		String execution= "";
		Queue<Object> parameterqueue = new LinkedList();
		// check if parameters were entered and create parameters array
		if (splitted.length > 1) {
			Object[] parameters = Arrays.copyOfRange(splitted, 1, splitted.length);
			parameterqueue = new LinkedList<>(Arrays.asList(parameters));
		}
		// check if the command includes an execution
		int index_dot = command.indexOf(WINDOW_PIPE_CHAR);
		if(index_dot!=-1) {
			execution=command.substring(index_dot+1);
			command =command.substring(0,index_dot);
		}
		StringBuilder sb = new StringBuilder(); // a place where a potential shortcut execution can be stored
		Command c = getCommand(command, commandDirs, usedWB, sb);
		if (c == null) {
			println(I18N.msg("error.command.notfound"));
			flush();
			return new Request();
		}
		if(sb.length()>0) { // if the shortCut contained a execution
			execution = sb.toString();
		}
		
		return new Request(query, c, commandDirs, execution, usedWB, parameterqueue, doLocklely);

	}
	


	/**
	 * getting a list of the available prefixes TODO: replaced by proper Registry
	 * command
	 * 
	 * @param wb
	 * @return
	 */

	private ArrayList<Character> getPrefiexes(Workbench wb) {
		ArrayList<Character> prefixes = new ArrayList();
		prefixes.add(APP_CHAR);
		prefixes.add(WINDOW_CHAR);
		return prefixes;

	}

	/**
	 * get the workbench used by the command-space of the prefix TODO: replaced by
	 * proper Registry command
	 * 
	 * @param prefix
	 * @return
	 */

	private ProjectWorkbench getUsedWB(char prefix) {
		switch (prefix) {
		case APP_CHAR:
			return getMainWorkbench();
		case WINDOW_CHAR:
			return selected;
		}
		return null;

	}

	/**
	 * get the command directories used by the command-space of the prefix TODO:
	 * replaced by proper Registry command
	 * 
	 * @param prefix
	 * @return
	 */

	private String[] getCommandDirs(char prefix) {
		switch (prefix) {
		case APP_CHAR:
			return APPCOMMANDDIRS;
		case WINDOW_CHAR:
			return WINDOWCOMMANDDIRS;
		}
		return new String[0];
	}

	/**
	 * Get a command from a query (load the command node in the registry) looking at
	 * the basDirs directories. The command is executed by the given workbench wb.
	 * The command can be executed in the background with the parameter "&" at the
	 * end of the query (e.g. $open xx.gs &). Commands executed in the background
	 * cannot request values from the ui. (i.e. ...).
	 * 
	 * @param query
	 * @param baseDirs
	 * @param wb
	 */
	private boolean handleRequest(Request r) {
		if (r.command==null || r.query.isBlank()) {return false;}
		if (r.doLocklely) {
			allowToWrite.lock();
			try {
				setState(State.READ_VALUE);
				Workbench.setCurrent(r.wb);
				Registry.setCurrent(r.wb.getRegistry());
				
				//run a execution on a panel of the command is a panel factory and a execution is given
				if( r.command instanceof PanelFactory & r.panelExec.length()>0) {
					Panel p = r.wb.getWindow().getPanel(((Item) r.command).getAbsoluteName());
					//if the panel can't be found it is initialized
					if(p==null) {
						r.command.run(Map.EMPTY_MAP, r.wb);
						p = r.wb.getWindow().getPanel(((Item) r.command).getAbsoluteName());
						if(p==null) { //the panel seems to not exsit
							println(I18N.msg("error.panel.notfound"));
							flush();
							return false;
						}
					}					
					if(p instanceof ExecutableComponent) {
						((ExecutableComponent)p).run(r.panelExec,r.paramqueue);
						return true;
					}
					println(I18N.msg("error.panel.notcli"));
					flush();
					return false;
				}
				
				r.command.run(r.paramqueue, r.wb);
			} finally {
				updateCLICompleters();
				setState(State.READ_COMMAND);
				allowToWrite.unlock();
			}
			return true;
		} else {
			UI.executeLockedly(r.wb.getRegistry().getProjectGraph(), true, r.command, r.paramqueue, r.wb,
					JobManager.ACTION_FLAGS);
			return true;
		}
	}
	

	/**
	 * query the base directories of the command space to find the command
	 * 
	 * @param name
	 * @param baseDirs
	 * @param wb
	 * @return
	 */

	private Command getCommand(String name, String[] baseDirs, Workbench wb, StringBuilder executionSB) {
		Item c = null;

		// if the command starts with a / it is assumed as a absolute path on the
		// registry
		if (name.charAt(0) == '/') {
			c = Item.resolveItem(wb.getRegistry(), name);
		} else {
			// loop over all given base directories to find the command
			for (String base : baseDirs) {
				c = Item.resolveItem(wb.getRegistry(), base + "/" + name);
				if (c != null) { // if a command is found exit
					break;
				}
			}
		}
		if (c instanceof Command) {
			return (Command) c;
		}
		if (c instanceof StringConst ) {
			String cname = (String)((StringConst)c).evaluate(c, null);
			if(cname!=null) {
				int index_dot = cname.indexOf(WINDOW_PIPE_CHAR);
				if(index_dot!=-1) {
					String execution=cname.substring(index_dot+1);
					executionSB.append(execution);
					cname =cname.substring(0,index_dot);
				}
				return(Command) Item.resolveItem(wb, cname);
			}
		}
		return null;
	}

	/**
	 * Open a project in a new workbench.
	 * 
	 * @param info: A String that is either the absolute path to a project to open,
	 *              or a relative path using the working directory as base. See more
	 *              {@link #changeDirectory(Object)}.
	 */
	public ProjectWorkbench open(Object info, Command afterCommand) {
		String path = Utils.getStringParameter(info, true);
		path = (path == null) ? null : Utils.getAbsolutePath(this.getCurrentWorkbench(), path).toString();
		FileChooserResult fr = getToolkit().chooseFile(UI.I18N.getString("filedialog.openproject", "Open Project"),
				IO.getReadableFileTypes(new IOFlavor[] { IOFlavor.PROJECT_LOADER }), Window.OPEN_FILE, true, null,
				selected, path);
		if (fr != null) {
			FileSource fs = fr.createFileSource(getRegistry(), null);
			try {
				ProjectWorkbench wb = open(fs, new ProjectFactoryImpl(), null);
				if (afterCommand!=null) {try {
					wb.execute(afterCommand);
				} catch (Exception e) {
					println("After command failed");
				}}
				return wb;
			} catch (IOException e) {
				print(I18N.msg("error.workbench.projectnotfound"));
			}
		}
		return null;
		// TODO:
		// addToLastUsed(this, fr.file);

	}
	
	public ProjectWorkbench open(Object info) {
		if(script) {executing=true;}
		return open(info, (script)? WAIT_EXECUTION: null);
	}
	
	public ProjectWorkbench open(FilterSource fs, Map initParams) {
		try {
			return open(fs, new ProjectFactoryImpl(), initParams);
		} catch (IOException e) {

			print(I18N.msg("error.workbench.projectnotfound"));
		}
		return null;
	}

	/**
	 * Not possible yet due to lock issues on graph
	 */
//	public void openProject(Object info) {
//		Object projectId = Utils.getParameter(info);
//		wbm.createWorkbench(projectId);
//	}
	
	
	/**
	 * Set a workbench as selected (i.e. current) from its id.
	 * 
	 * @param info: a String that represent the id of the workbench to select (i.e.
	 *              select as current workbench).
	 */
	protected void selectWorkbench(Object info) {
		int id;
		if (info instanceof Integer) {
			id = (int) info;
			selectWorkbench(wbm.getWorkbench(id));
		}else {
		try {
		id = Utils.getintParameter(info);
		selectWorkbench(wbm.getWorkbench(id));
		}catch(NullPointerException e) {		
			println(I18N.msg("error.workbench.notindex"));
			listWorkbenches(info);
		}catch(IndexOutOfBoundsException e) {
			println(I18N.msg("error.workbench.index"));
			listWorkbenches(info);
		}catch(NumberFormatException e) {
			println(I18N.msg("error.workbench.notanumber"));
			listWorkbenches(info);
		}
		}
	}

	/**
	 * print the list of currently opened workbenches by the application.
	 * 
	 * @param info: not used -
	 */
	// TODO: return the list List<Workbench>?
	public void listWorkbenches(Object info) {
		HashMap<Integer,Workbench> workbenches = wbm.listWorkbenches();
		String desc = "";
		Utils.consoleWrite(selected, "ID: \tWorkbench Name \tStatus");
		for (HashMap.Entry<Integer,Workbench> e : workbenches.entrySet()) {
			desc = String.valueOf(e.getKey()) + " :"+ "\t" +
				e.getValue().getName() + "\t\t" + 
				((e.getValue().getStatus() == null) ? "Waiting" : e.getValue().getStatus() + "\t\t"+
				((e.getValue().getProgress() == null) ? "Done" : e.getValue().getProgress().floatValue() * 100 + "%"));
			Utils.consoleWrite(selected, desc);
		}
	}

	/**
	 * print the list of currently opened projects by the application.
	 * 
	 * @param info: not used -
	 */
	// TODO: info could be some additional parameters.
	// TODO: return the list List<Project>?
	public void listProjects(Object info) {		
		Utils.consoleWriteList(selected, pm.listOpenProjects(), false);
	}

	public static void runXLConsole(Item item, Object info, Context ctx) {
		((CLIApplication) ctx.getWorkbench().getApplication()).runXLConsole();
	}
	
	private void runXLConsole() {
		setState(State.READ_XLCONSOLE);
		String line = "";
		CLIConsolePanel c = (CLIConsolePanel) PanelFactory.getAndShowPanel(selected, "/ui/panels/rgg/console",
				Map.EMPTY_MAP);
		if (c==null) {
			println(I18N.msg("error.xlconsole.panelnotfound"));
			flush();
			return;
		}
		println(I18N.msg("xlconsole.exit.msg", XLEXIT));
		flush();
		getTerminalInput();
	}
	
	

	/**
	 * Close a workbench and deregister (disconnect) its project.
	 */
	@Override
	public void close(Object info) {
		//TODO/
		//get wb from info
		//wbm.closewb(wb)
		System.out.println("todo");
	}
	
	
	public static void closeWB(Item item, Object info, Context ctx) {
		((CLIApplication) ctx.getWorkbench().getApplication()).closeWB(info);
	}
	
	
	private void closeWB(Object info) {
		try {
			int id = Utils.getintParameter(info);
			wbm.closeWorkbench(wbm.getWorkbench(id));
			}catch(NullPointerException e) {		
				println(I18N.msg("error.workbench.notindex"));
				listWorkbenches(info);
			}catch(IndexOutOfBoundsException e) {
				println(I18N.msg("error.workbench.index"));
				listWorkbenches(info);
			}catch(NumberFormatException e) {
				println(I18N.msg("error.workbench.notanumber"));
				listWorkbenches(info);
			}
	}
	

	
	public void create(Object info, Command afterCommand) throws IOException {
		String name;
		try {
		name = Utils.getStringParameter(info, false);
		}catch(NullPointerException e) {
			name="newRGG";
		}
		String newName = Utils.getStringParameter(info, true);
		Object id =create(name,newName);
		if(id==null) {
			println("Template not found");
			flush();
			return;
		}
		if (afterCommand!=null) {try {
			wbm.getWorkbench(id).execute(afterCommand);
		} catch (Exception e) {
			println("After command failed");
		}}
	}
	
	/**
	 * Create a new workbench with a new project.
	 * 
	 * @throws IOException
	 */
	@Override
	public void create(Object info) throws IOException{
		if(script) {executing=true;}
		create(info, (script)? WAIT_EXECUTION: null);
	}
 
	public void loadExample(Object info, Command afterCommand) {
		try {
			String name = Utils.getStringParameter(info, false);
			String newName = Utils.getStringParameter(info, true);
			Object id= loadExample(name,newName);
			if(id==null) {
				println("Example not found");
				flush();
			}
			else if (afterCommand!=null) {try {
				wbm.getWorkbench(id).execute(afterCommand);
			} catch (Exception e) {
				println("After command failed");
			}}
		} catch (Exception e) {
		}
	}
	
	
	@Override 
	public void loadExample(Object info) {
		if(script) {executing=true;}
		loadExample(info, (script)? WAIT_EXECUTION: null);
	}
	
	
	// Shouldn't this be in Utils?
	@Override
	public void listExamples(Object info) {
		listFilterSourceFactorys(info,"/examples");
	}
	
	@Override
	public void listTemplates(Object info) {
		listFilterSourceFactorys(info,"/ui/templates");
	}

	
	
	private void listFilterSourceFactorys(Object info, String path) {
		String name = Utils.getStringParameter(info, true);
		
		Item fileDir = Item.resolveItem(selected.getRegistry().getRootRegistry(), path);
		Item[] files = Item.findAll(fileDir, ItemCriterion.INSTANCE_OF, ProjectDirectory.class, true);
		for (Item f : files) {
			if(name==null || f.getName().toLowerCase().contains(name.toLowerCase()) || f.getDescription(Described.NAME).toString().toLowerCase().contains(name.toLowerCase()) || f.getDescription(Described.SHORT_DESCRIPTION).toString().toLowerCase().contains(name.toLowerCase())) {
				String part = "["+f.getName()+"]: "+ f.getDescription(Described.NAME)+"\n";
				part+=f.getDescription(Described.SHORT_DESCRIPTION);
				part+="\n";
				println(part);
			}
		}
		flush();
		
	}
	
	/**
	 * Terminate the application. It closes all opened workbenches managed by its
	 * wbm. Close workbenches do not necessarily close the projects.
	 */
	@Override
	public void stop() {
		// TODO Auto-generated method stub
		System.out.println("todo");
	}

	public Completer getSysFileCompleter() {
		return sysFileCompleter;
	}
	
	
	public static void runScript(Item item, Object info, Context ctx) {
		((CLIApplication) ctx.getWorkbench().getApplication()).runScript(info);
	}
	
	
	boolean script=false;
	/**
	 * read a script file (a text file with groimp commands separated with new line) and push all commands 
	 * to a workbench
	 * 
	 * Be carefull, opening and compiling workbenches is not properly handled and can lead to deadlock or execution over nonexisting graph
	 * @param info
	 */
	private void runScript(Object info) {
		script=true;
		try {
			String scriptPath = Utils.getStringParameter(info, false);
			String workbenchId = Utils.getStringParameter(info, true);
			CLIWorkbench wb;
			CLIWorkbench compiledwb;
			
			if (workbenchId == null) {
				wb=(CLIWorkbench) selected;
			}
			else {
				try {
					wb = (CLIWorkbench) wbm.getWorkbench(Integer.valueOf( workbenchId));
				} catch(Exception e) {
					print(I18N.msg("error.workbench.projectnotfound"));
					return;
				}
			}
			
			compiledwb = wb;
			scriptPath = Utils.getAbsolutePath(this.getCurrentWorkbench(), scriptPath).toString();
			FileSource fs = new FileSource (new File(scriptPath), MimeType.TEXT_PLAIN, getRegistry(), null);
			try (BufferedReader br = new BufferedReader(new InputStreamReader(fs.getInputStream()))) {
		        String line;
		        while ((line = br.readLine()) != null) {
		        	synchronized(getExecutionLock()) {
			        	if (isExecuting()) { // wait for a wb to be open/created. before pushing tasks onto them
		        		try {
		        			getExecutionLock().wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
			        }
		        	
		    		synchronized(compiledwb.getCompilationLock()) {
		    		if (compiledwb.isCompiling()) { //special waiting block for the compilation as it does not prevent run to happens || handled here otherwise the jm of hte wb loop infinitely
		            	synchronized(compiledwb.getCompilationLock()) {
		    	        	try {
		    	        		compiledwb.getCompilationLock().wait();
		    				} catch (InterruptedException e) {
		    					e.printStackTrace();
		    				}
		    	        }
		            }
		    		}
				        Request r = parseRequest(line);
				        println("dispatched : " + line + " on " + r.wb);
				        flush();
				        if (r.wb==getMainWorkbench()) {r.doLocklely=true;} //the command on the mainworkbench (usually open, load, ...) are done on locking the app
				        else {r.doLocklely=false;} // all other commands are dispatched on their wb.
				        // ugly case were you compile - block the script until done because the jm of workbenches do not manage it .....
				        if (r.query.trim().equals("compile")) {r.doLocklely=true; compiledwb=(CLIWorkbench) r.wb;}// make sure the script is following the current wb
				        handleRequest(r);
		        	}
		        }
			}
			println("Script completely dispatched.");
		} catch (IOException e) {
			print("File cannot be read");
			flush();
		}
		catch (NullPointerException e) {
			print("Enter a file path");
			flush();
		}
		script=false;
	}
	
	
	/**
	 * Extremely bad workaround - all of that should be handle by a proper jobmanager
	 */
	Command afterCommand = null;
	boolean executing = false;
	Object EXECUTING_LOCK = new Object();
	Command WAIT_EXECUTION = new Command() {
		@Override
		public String getCommandName() {
			return null;
		}
		@Override
		public void run(Object info, Context context) {
			synchronized(EXECUTING_LOCK) {
				executing = false;
				EXECUTING_LOCK.notifyAll();
			}
		}
	};
	
	public boolean isExecuting() {
		return executing;
	}
	
	// Should check for right, not give the lock right  away
	public Object getExecutionLock() {
		return EXECUTING_LOCK;
	}
	
}