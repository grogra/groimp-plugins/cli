package de.grogra.cli.ui;

public class CLIRootPane extends CLIContainer implements ICLIPanel
{
	protected CLIPanelSupport support;
	private CLIContainer contentpane;

	
	public CLIRootPane() {
		setContentPane(new CLIContainer());
	}

	public CLIPanelSupport getSupport ()
	{
		return (support != null) ? support : getParent().getSupport();
	}


	public void initialize (CLIPanelSupport support, de.grogra.util.Map p)
	{
		this.support = support;
	}


	public void setMenu (CLIComponent menu)
	{
	}


	public void dispose ()
	{
	}

	@Override
	public CLIContainer getContentPane() {
		return contentpane;
	}
	
	public void setContentPane(CLIContainer c) {
		contentpane = c;
	}

}
