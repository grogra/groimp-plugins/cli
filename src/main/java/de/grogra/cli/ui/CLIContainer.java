package de.grogra.cli.ui;

import java.util.ArrayList;
import de.grogra.pf.ui.ComponentWrapper;


public class CLIContainer extends CLIComponent{
	
	java.util.List<CLIComponent> component = new ArrayList<>();
	transient int descendantsCount;
	
	public CLIContainer() {
	}
	
	public CLIContainer(ComponentWrapper comp) {
		this.add((CLIComponent)comp);
	}
	
	@Override
	public void dispose() {
		for(CLIComponent c : component) {
			c.dispose();
		}
	}

	@Override
	public CLIComponent getComponent() {
		return this;
	}
		
	public CLIComponent getComponent(int n) {
		try {
            return component.get(n);
        } catch (IndexOutOfBoundsException z) {
            throw new ArrayIndexOutOfBoundsException("No such child: " + n);
        }
	}
	
	/**
     * Checks that the component
     * isn't supposed to be added into itself.
     */
    private void checkAddToSelf(CLIComponent comp){
        if (comp instanceof CLIContainer) {
            for (CLIContainer cn = this; cn != null; cn=cn.parent) {
                if (cn == comp) {
                    throw new IllegalArgumentException("adding container's parent to itself");
                }
            }
        }
    }

	public int countComponents() {
        return component.size();
    }
	
	public CLIComponent add(ComponentWrapper comp) {
		CLIComponent c = new CLIComponent();
		c.setContent(comp);
        addImpl(c, null, -1);
        return c;
    }
	
	public CLIComponent add(CLIComponent comp) {
        addImpl(comp, null, -1);
        return comp;
    }
	
	public void add(CLIComponent comp, Object constraints) {
        addImpl(comp, constraints, -1);
    }

	public void add(CLIComponent comp, Object constraints, int index) {
		addImpl(comp, constraints, index);
	}
	
	protected void addImpl(CLIComponent comp, Object constraints, int index) {
        synchronized (getTreeLock()) {
            /* Check for correct arguments:  index in bounds,
             * comp cannot be one of this container's parents,
             * and comp cannot be a window.
             * comp and container must be on the same GraphicsDevice.
             * if comp is container, all sub-components must be on
             * same GraphicsDevice.
             */
            if (index > component.size() || (index < 0 && index != -1)) {
                throw new IllegalArgumentException(
                          "illegal component position");
            }
            checkAddToSelf(comp);
            /* Reparent the component and tidy up the tree's state. */
            if (comp.parent != null) {
                comp.parent.remove(comp);
                if (index > component.size()) {
                    throw new IllegalArgumentException("illegal component position");
                }
            }

            //index == -1 means add to the end.
            if (index == -1) {
                component.add(comp);
            } else {
                component.add(index, comp);
            }
            comp.parent = this;
        }
    }
	
	public void remove(CLIComponent comp) {
        synchronized (getTreeLock()) {
            if (comp.parent == this)  {
                int index = component.indexOf(comp);
                if (index >= 0) {
                    remove(index);
                }
            }
        }
    }
	
	public void remove(int index) {
        synchronized (getTreeLock()) {
            if (index < 0  || index >= component.size()) {
                throw new ArrayIndexOutOfBoundsException(index);
            }
            CLIComponent comp = component.get(index);

            adjustDescendants(-(comp.countHierarchyMembers()));

            comp.parent = null;
            component.remove(index);
        }
    }
	
	// Should only be called while holding tree lock
    void adjustDescendants(int num) {
        if (num == 0)
            return;

        descendantsCount += num;
        adjustDescendantsOnParent(num);
    }
    
 // Should only be called while holding tree lock
    void adjustDescendantsOnParent(int num) {
        if (parent != null) {
            parent.adjustDescendants(num);
        }
    }
    
    public void show() {
    	for(CLIComponent c : component) {
			c.show();
		}
    }
    
    @Override
    public void run(String cmd, Object info) {
    	super.run(cmd, info);
    	for(CLIComponent c : component) {
			c.run(cmd, info);
		}
    }

}
