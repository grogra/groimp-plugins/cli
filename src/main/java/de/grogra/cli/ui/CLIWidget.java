package de.grogra.cli.ui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.tree.TreeModel;

import de.grogra.pf.ui.tree.UITree;
import de.grogra.pf.ui.util.WidgetBase;
import de.grogra.reflect.Type;
import de.grogra.util.Described;
import de.grogra.util.Map;
import de.grogra.util.Quantity;

public class CLIWidget extends WidgetBase {

	Object component;
	String type="";
	List<String> possibleValues= new ArrayList<String>();
	boolean editable=false;
	
	public CLIWidget() {
	}
		
	//numeric
	public CLIWidget(Type type, Quantity quantity, Map params) {
		this.type="Number";
		initComponent();
		setEditable(true);
	}
	
	//string
	public CLIWidget(String type) {
		this.type=type;
		initComponent();
		setEditable(true);
	}
	
	//tree choice
	public CLIWidget(UITree tree) {
		this.type="Tree";
		getPossibleValues(tree);
		initComponent();
	}
	
	//choice
	public CLIWidget(ListModel list, boolean forMenu) {
		this.type="Choice";
		getPossibleValues(list);
		initComponent();
	}
	
	//boolean
	public CLIWidget(boolean forMenu, Map params) {
		this.type="Boolean";
		initComponent();
		setEditable(true);
	}
		
	private void getPossibleValues(UITree tree) {
		getUITreeElements(tree, tree.getRoot());
	}
	
	private void getPossibleValues(ListModel list) {
		
		Object o = new Object();
	}
	
	private void initComponent(){
		StringBuffer description=new StringBuffer();
		if (!this.type.isEmpty())
			description.append(this.type.toString());
		if (!this.possibleValues.isEmpty() ) {
			description.append(" - (");
			for (String s : this.possibleValues) {
				description.append(s+", ");
			}
			description.append(")");
		}
//		this.component=description.toString();
	}
	
	
	public void setEditable(boolean e) {
		this.editable=e;
	}
	
	public boolean getEditable() {
		return this.editable;
	}
	
	@Override
	public void setEnabled(boolean enabled) {
	}

	@Override
	public void updateValue(Object value) {
	}

	@Override
	public Object getComponent() {
		return this;
	}
	
	@Override
	protected void setComponentValue(Object value) {
	}
	

	private void getUITreeElements(TreeModel model, Object object) {
		String value = getUINodeValue(model, object);
    	if (!value.isEmpty() ) {
    		possibleValues.add(value);
    	}
	    for (int i = 0; i < model.getChildCount(object); i++) {
	        getUITreeElements(model, model.getChild(object, i));
	    }
	}
	
	private String getUINodeValue(TreeModel model, Object item) {
		return String.valueOf (((UITree) model).getDescription
				   (item, Described.NAME));
	}
	
}
