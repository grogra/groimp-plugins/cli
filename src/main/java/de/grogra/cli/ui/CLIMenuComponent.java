package de.grogra.cli.ui;

import java.util.EventObject;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.tree.UITree;
import de.grogra.util.Described;

public class CLIMenuComponent extends CLIComponent implements UITree {
	
	@Override
	public void dispose() {
		((UITree)getContent()).dispose();
	}

	@Override
	public CLIComponent getComponent() {
		return this;
	}
	
	public CLIMenuComponent(UITree t){
		setName("menu");
		setShowable(false);
		setContent(t);
	}

	private String getNodeText(Object item) {
		return String.valueOf (getDescription(item, Described.NAME));
	}
	
	public Object getNodeFromName(TreeModel model, Object node, String name) {
		if (name.equals(getNodeText(node)))
			return node;
		for (int i = 0; i < model.getChildCount(node); i++) {
			getNodeFromName(model, model.getChild(node, i), name );
	    }
		return null;
	}

	@Override
	public Object getRoot() {
		return ((UITree)getContent()).getRoot();
	}

	@Override
	public Object getChild(Object parent, int index) {
		return ((UITree)getContent()).getChild(parent, index);
	}

	@Override
	public int getChildCount(Object parent) {
		return ((UITree)getContent()).getChildCount(parent);
	}

	@Override
	public boolean isLeaf(Object node) {
		return ((UITree)getContent()).isLeaf(node);
	}

	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {
		((UITree)getContent()).valueForPathChanged(path, newValue);
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		return ((UITree)getContent()).getIndexOfChild(parent, child);
	}

	@Override
	public void addTreeModelListener(TreeModelListener l) {
		((UITree)getContent()).addTreeModelListener(l);
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
		((UITree)getContent()).removeTreeModelListener(l);
	}

	@Override
	public boolean nodesEqual(Object a, Object b) {
		return ((UITree)getContent()).nodesEqual(a, b);
	}

	@Override
	public int getType(Object node) {
		return ((UITree)getContent()).getType(node);
	}

	@Override
	public String getName(Object node) {
		return ((UITree)getContent()).getName(node);
	}

	@Override
	public boolean isAvailable(Object node) {
		return ((UITree)getContent()).isAvailable(node);
	}

	@Override
	public boolean isEnabled(Object node) {
		return ((UITree)getContent()).isEnabled(node);
	}

	@Override
	public Object resolveLink(Object node) {
		return ((UITree)getContent()).resolveLink(node);
	}

	@Override
	public Object getDescription(Object node, String type) {
		return ((UITree)getContent()).getDescription(node, type);
	}

	@Override
	public void eventOccured(Object node, EventObject event) {
		((UITree)getContent()).eventOccured(node, event);
	}

	@Override
	public Object invoke(Object node, String method, Object arg) {
		return ((UITree)getContent()).invoke(node, method, arg);
	}

	@Override
	public void update() {
		((UITree)getContent()).update();
	}

	@Override
	public Object getParent(Object child) {
		return ((UITree)getContent()).getParent(child);
	}

	@Override
	public Context getContext() {
		return ((UITree)getContent()).getContext();
	}
}
