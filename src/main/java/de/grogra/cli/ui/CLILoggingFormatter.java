package de.grogra.cli.ui;


/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


import java.awt.Dimension;
import java.util.logging.*;
import de.grogra.util.DetailedException;
import de.grogra.util.I18NBundle;
import de.grogra.util.UserException;

public class CLILoggingFormatter extends Formatter
{
	protected final Dimension iconSize;
	protected final I18NBundle i18n;

	
	public CLILoggingFormatter (I18NBundle i18n, Dimension iconSize)
	{
		this.i18n = i18n;
		this.iconSize = iconSize;
	}

	
	@Override
	public String format (LogRecord log)
	{
		StringBuffer buffer = new StringBuffer (System.lineSeparator());
		int p = buffer.length ();
		de.grogra.util.Utils.formatDateAndName (log, buffer);
		de.grogra.util.Utils.escapeForXML (buffer, p);
		Level l = log.getLevel ();
		int lv = l.intValue ();
		String key;
		buffer.append (System.lineSeparator());
		if (lv < Level.WARNING.intValue ())
		{
			key = "log.info";
			buffer.append("#Info");
		}
		else if (lv < Level.SEVERE.intValue ())
		{
			key = "log.warning";
			buffer.append("#Warning");
		}
		else
		{
			key = "log.severe";
			buffer.append("#Severe");
		}
		buffer.append (System.lineSeparator()+"-------------------------------"+System.lineSeparator());
		if (log.getMessage ().length () > 0)
		{
			p = buffer.length ();
			buffer.append (log.getMessage ());
			if (log.getMessage ().startsWith ("<html>"))
			{
				buffer.delete (p, p + 6);
				p = buffer.lastIndexOf ("</html>");
				if (p >= 0)
				{
					buffer.delete (p, p + 7);
				}
			}
			else
			{
				de.grogra.util.Utils.escapeForXML (buffer, p);
				for (int i = buffer.length () - 1; i >= p; i--)
				{
					if (buffer.charAt (i) == '\n')
					{
						buffer.replace (i, i + 1, System.lineSeparator());
					}
				}
			}
			buffer.append (System.lineSeparator());
		}
		Throwable t = de.grogra.util.Utils.getMainException (log.getThrown ());
		if (t instanceof DetailedException)
		{
			buffer.append (((DetailedException) t).getDetailedMessage (false));
		}
		else if (t instanceof UserException)
		{
			p = buffer.length ();
			buffer.append (t.getLocalizedMessage ());
			de.grogra.util.Utils.escapeForXML (buffer, p);
		}
		else if (t != null)
		{
			Throwable u = t;
			while (u != null)
			{
				p = buffer.length ();
				String s = u.getClass ().getName ();
				buffer.append (s.substring (s.lastIndexOf ('.') + 1));
				s = u.getLocalizedMessage ();
				if (s != null)
				{
					buffer.append (": ").append (s);
				}
				de.grogra.util.Utils.escapeForXML (buffer, p);
				buffer.append (System.lineSeparator());
				u = u.getCause ();
				if (u != null)
				{
					buffer.append ("Caused by ");
				}
			}
			buffer.append (System.lineSeparator()+"Stack Trace:"+System.lineSeparator());
			p = buffer.length ();
			String XLerrorsInp=de.grogra.util.Utils.getStackTrace (t);
			XLerrorsInp=XLerrorsInp.replaceAll("<br>", System.lineSeparator());
			XLerrorsInp=XLerrorsInp.replaceAll("<pre>", "");
			XLerrorsInp=XLerrorsInp.replaceAll("</pre>", "");
			String XLerrors="";
			for(String line: XLerrorsInp.split("\n")) {
				if(line.startsWith("<a")) {
					line=line.substring(line.indexOf('>'));
					line=line.substring(line.indexOf('<'));
				}
				XLerrors+=line+System.lineSeparator();
			}
			
			
			buffer.append (XLerrors);
		}
		return buffer.append (System.lineSeparator()).toString ();
	}


	@Override
	public String getHead (Handler h)
	{
		return "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<";
    }


	@Override
	public String getTail (Handler h)
	{
		return ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>";
	}

}
