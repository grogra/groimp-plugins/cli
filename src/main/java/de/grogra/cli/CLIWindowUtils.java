package de.grogra.cli;

import de.grogra.cli.ui.CLIWindowSupport;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.Panel;

public class CLIWindowUtils {

	/**
	 * List all the panels opened in this window
	 */
	public static void listPanels(Item item, Object info, Context ctx) {
		String allPanels = "";
		int i = 0;
		for (Panel p : ctx.getWindow().getPanels(null)) {
			allPanels+= "["+String.valueOf(i)+"] : "+p.getPanelId()+System.lineSeparator();
			i++;
		}
		((CLIWindowSupport)ctx.getWindow()).consoleWrite(allPanels);
	}
	
}
