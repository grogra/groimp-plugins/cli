package de.grogra.cli;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;

import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.RegistryLoader;
import de.grogra.pf.registry.Directory;
import de.grogra.pf.registry.Executable;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.Project;
import de.grogra.pf.ui.ProjectWorkbench;
import de.grogra.pf.ui.UIApplication;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.WorkbenchManager;
import de.grogra.pf.ui.registry.ProjectFactory;
import de.grogra.projectmanager.JobManagerImpl;
import de.grogra.util.Map;
import de.grogra.util.StringMap;

public class CLIWorkbenchManager implements WorkbenchManager {
	private Registry registry;
	private HashMap<Integer,Workbench> workbenches;
	protected UIApplication app;
	private static int wbID;

	public CLIWorkbenchManager(Registry r, UIApplication app) {
		try
		{
			loadRegistry (r, null, false);
		}
		catch (IOException e)
		{
			throw new AssertionError (e);
		}
		r.setEmptyGraph ();
		registry = r;
		workbenches = new HashMap<Integer,Workbench>(); // list of all workbenches of the UI app
		this.app = app;
		wbID =0;
	}

	@Override
	public Registry getRegistry() {
		return registry;
	}

	public Object getWorkbenchId(ProjectWorkbench wb) {
		for(int key : workbenches.keySet()) {
			if(workbenches.get(key).equals(wb)) {
				return key;
			}
		}
		return null;
	}

	/**
	 * register the workbench to the manager Should probably be done with a lock to
	 * prevent asynchronous changes
	 */
	@Override
	public void registerWorkbench(ProjectWorkbench wb) {
		workbenches.put(wbID, wb);
		wb.setToken(wb.getApplicationName()+"-"+(wbID++));
	}

	/**
	 * unregister the workbench to the manager
	 */
	@Override
	public void deregisterWorkbench(ProjectWorkbench wb) {
		synchronized (workbenches){
		if (isManager(wb)) {
			workbenches.remove(((int)getWorkbenchId((ProjectWorkbench) wb)));
			if(app.getMainWorkbench()==wb) {
				app.exit();
			}else if(app.selected==wb) {
				app.selected=(ProjectWorkbench) app.getMainWorkbench();
			}
		}
		}
	}

	/**
	 * return the workbench as with given id
	 */
	@Override
	public ProjectWorkbench getWorkbench(Object id) throws IndexOutOfBoundsException{
		return (ProjectWorkbench) workbenches.get((int) id);
	}

	/**
	 * return the list of managed workbenches handled by this manager
	 */
	@Override
	public HashMap<Integer, Workbench> listWorkbenches() {
		return workbenches;
	}
	
	
	/**
	 * return the list of managed workbenches handled by this manager
	 */
	public String[] listWorkbenchesName() {
		int l = workbenches.size();
		if (l == 0) {
			return new String[0];
		}
		int i = 0;
		String[] list = new String[l];
		for (int key : workbenches.keySet()) {
			list[i] = key+" : "+workbenches.get(key).getName(); 
			i++;
		}
		return list;
	}

	/**
	 * close a workbench
	 */
	@Override
	public void closeWorkbench(ProjectWorkbench w) {
		closeWorkbench(w,null);
	}
	
	
	/**
	 * close a workbench
	 */
	@Override
	public void closeWorkbench(ProjectWorkbench w, Command afterDispose) {
		deregisterWorkbench(w);
		Command kill = new Command() {
			@Override
			public String getCommandName() {
				return null;
			}
			@Override
			public void run(Object info, Context context) {
				context.getWorkbench().getJobManager().stop(afterDispose);
				

			}
		};
		w.getJobManager().execute(kill, w, w, JobManager.ACTION_FLAGS);
	}

	/**
	 * chekc if the given workbench is handled by this manager
	 * 
	 * @param workbench whose ownership is to check
	 */
	@Override
	public boolean isManager(ProjectWorkbench wb) {
		return workbenches.values().contains(wb);
	}

	
	/**
	 * Create a new wb around an loaded project
	 */
	@Override
	public ProjectWorkbench createWorkbench(ProjectFactory pf, FilterSource fs,	Map initParams) 
	{
		try {
			Project p = app.getProjectManager().openProject(pf, fs, initParams);
			return createWorkbench(p);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	

	@Override
	public ProjectWorkbench createWorkbench(ProjectFactory pf) throws IOException {
		
		Project p =app.getProjectManager().createProject( pf);
		return createWorkbench(p);
	}

	@Override
	public ProjectWorkbench createWorkbench(Object projectId) {
		Project p = app.getProjectManager().getProject(projectId);
		return createWorkbench(p);

	}

	protected ProjectWorkbench createWorkbench(Project p) {
		CountDownLatch doneSignal = new CountDownLatch(1);		
		JobManager jm = new JobManagerImpl(doneSignal);
		CLIWorkbench wb =new CLIWorkbench(Registry.create(p.getRegistry()), jm, app, Map.EMPTY_MAP);
		wb.setProject(p);
		wb.init();
		
		wb.getJobManager().initialize(wb);
		registerWorkbench(wb);
		app.getProjectManager().linkWorkbenchToProject(p, wb);
		new Thread((JobManagerImpl) wb.getJobManager(), "Workbench@Id" + getWorkbenchId(wb)).start();
		try {
			doneSignal.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return wb;
	}
	
	
	private static void loadRegistry(Registry reg, RegistryLoader loader, boolean project) throws IOException {
		StringMap m = new StringMap().putObject("registry", reg);
		if (project) {
			((Item) reg.getRoot()).add(new Directory("project"));
		}
		Executable.runExecutables(reg.getRootRegistry(), "/hooks/configure", reg, m);
		if (loader != null) {
			loader.loadRegistry(reg);
		}
		Executable.runExecutables(reg.getRootRegistry(), "/hooks/complete", reg, m);
		reg.activateItems();
	}

	@Override
	public ArrayList<Workbench> getWorkbenches() {
		return  new ArrayList<Workbench>(workbenches.values());
	}

}
