## GroIMP Command Line Interface (CLI)

A simple CLI for GroIMP. It enables to start GroIMP in command line with the parameter '-a cli'. Then, project can be open/modified/run/exported with commands: $open $saveas $run. 

